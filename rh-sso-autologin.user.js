// ==UserScript==
// @name        Red Hat SSO autologin
// @namespace   Violentmonkey Scripts
// @match       https://sso.redhat.com/auth/realms/redhat-external/login-actions/authenticate
// @grant       none
// @version     1.0.1
// @author      Peter Krempa
// @description 2023/09/14
// @run-at      document-end
// ==/UserScript==

(function(){
    "use strict";
    $('#username-verification').val('pkrempa@redhat.com');
    $('#login-show-step2').click();
})();
