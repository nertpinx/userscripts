// ==UserScript==
// @name        rh-workday
// @namespace   Violentmonkey Scripts
// @match       https://wd5.myworkday.com/*
// @grant       none
// @version     1.0
// @author      Peter Krempa
// @description 2023/09/22
// ==/UserScript==

(function() {
    "use strict";

    const evt = new Event("mousemove", { bubbles: true, cancelable: false });

    function bump() {
        document.getElementsByTagName("html")[0].dispatchEvent(evt);
    }

    setInterval(bump, 60000);
})();
